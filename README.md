# SQL technical paper

## Name
SQL Technical paper on importent topics

## Description

SQL is used for accessing and managing data in relational databases. This article will teach you SQL basics and help you understand various topics of Structured Query Language.
## The Topics we will cover in this article are

* ACID
* CAP Theorem
* Joins (should be covered in drills)
* Aggregations, Filters in queries (should be covered in Drills)
* Normalization
* Indexes
* Transactions
* Locking mechanism
* Database Isolation Levels
* Triggers

