# TECHNICAL PAPER ON SQL AND DATABASE 


## ABSTRACT
SQL is used for accessing and managing data in relational databases. This article will teach you SQL basics and help you understand various topics of Structured Query Language.


## The Topics we will cover in this article are

* ACID
* CAP Theorem
* Joins (should be covered in drills)
* Aggregations, Filters in queries (should be covered in Drills)
* Normalization
* Indexes
* Transactions
* Locking mechanism
* Database Isolation Levels
* Triggers


## ACID properties
In order to maintain consistency in a database, before and after the transaction, certain properties are followed. These are called ACID properties. 

<br>
<img src="./images/ACID-Properties.jpg"/>

<br>



1. ### Atomicity 
   By this, we mean that either the entire transaction takes place at once or doesn’t happen at all. There is no midway i.e. transactions do not occur partially. Each transaction is considered as one unit and either runs to completion or is not executed at all
2. ### Consistency 
    This means that integrity constraints must be maintained so that the database is consistent before and after the transaction
3. ### Isolation
   This property ensures that multiple transactions can occur concurrently without leading to the inconsistency of the database state. Transactions occur independently without interference. 
4. ### Durability
   This property ensures that once the transaction has completed execution, the updates and modifications to the database are stored in and written to disk and they persist even if a system failure occurs. These updates now become permanent and are stored in non-volatile memory. The effects of the transaction, thus, are never lost. 

## CAP Theorem
The CAP theorem, originally introduced as the CAP principle, can be used to explain some of the competing requirements in a distributed system with replication. It is a tool used to make system designers aware of the trade-offs while designing networked shared-data systems. 

The three letters in CAP refer to three desirable properties of distributed systems with replicated data: consistency (among replicated copies), availability (of the system for read and write operations) and partition tolerance (in the face of the nodes in the system being partitioned by a network fault). 

The CAP theorem states that it is not possible to guarantee all three of the desirable properties – consistency, availability, and partition tolerance at the same time in a distributed system with data replication. 

The theorem states that networked shared-data systems can only strongly support two of the following three properties: 
 

1. ### Consistency 
    Consistency means that the nodes will have the same copies of a replicated data item visible for various transactions. A guarantee that every node in a distributed cluster returns the same, most recent and a successful write. Consistency refers to every client having the same view of the data. There are various types of consistency models. Consistency in CAP refers to sequential consistency, a very strong form of consistency. 
 
2. ### Availability 
    Availability means that each read or write request for a data item will either be processed successfully or will receive a message that the operation cannot be completed. Every non-failing node returns a response for all the read and write requests in a reasonable amount of time. The key word here is “every”. In simple terms, every node (on either side of a network partition) must be able to respond in a reasonable amount of time. 
 
3. ### Partition Tolerance 
    Partition tolerance means that the system can continue operating even if the network connecting the nodes has a fault that results in two or more partitions, where the nodes in each partition can only communicate among each other. That means, the system continues to function and upholds its consistency guarantees in spite of network partitions. Network partitions are a fact of life. Distributed systems guaranteeing partition tolerance can gracefully recover from partitions once the partition heals. 
 
The use of the word consistency in CAP and its use in ACID do not refer to the same identical concept. 

In CAP, the term consistency refers to the consistency of the values in different copies of the same data item in a replicated distributed system. In ACID, it refers to the fact that a transaction will not violate the integrity constraints specified on the database schema


##  Joins 
A JOIN clause is used to combine rows from two or more tables, based on a related column between them.
Here are the different types of the JOINs in SQL:

* (INNER) JOIN: Returns records that have matching values in both tables
* LEFT (OUTER) JOIN: Returns all records from the left table, and the matched records from the right table
* RIGHT (OUTER) JOIN: Returns all records from the right table, and the matched records from the left table
* FULL (OUTER) JOIN: Returns all records when there is a match in either left or right table
 


        Example

        SELECT Orders.OrderID, Customers.CustomerName, Orders.OrderDate
        FROM Orders
        INNER JOIN Customers ON Orders.CustomerID=Customers.CustomerID;

 
  <br>
  <img src="./images/Screenshot%20from%202022-06-30%2011-10-38.png"/>
<br>

##  Aggregations, Filters in queries

1. ###  Aggregations
     - SQL can apply different aggregation functions.they are 
       - COUNT,AVG,SUM,MIN,MAX,STDDEV
     -  This query demonstrates more of them:
  
            SELECT COUNT(ingredient_price) as count,
            AVG(ingredient_price) as avg,
            SUM(ingredient_price) as sum,
            MIN(ingredient_price) as min,
            MAX(ingredient_price) as max,
            STDDEV(ingredient_price) as stddev,

            NOTE :-( STDDEV - Standard deviation)
    

  - here that when you leave out the GROUP BY class, but include an aggregation function, SQL assumes that you want to group all rows together.

2. ### Filters
     - SQL filters are text strings that you use to specify a subset of the data items in an internal or SQL database data type
     - For SQL database and internal data types, the filter is an SQL WHERE clause that provides a set of comparisons that must be true in order for a data item to be returned. These comparisons are typically between field names and their corresponding values.
     - The SQL filter syntax supports the following comparative operators:
     
       *  `>, <, =, <=, =>, !=, LIKE, AND, OR, NOT`


## Normalization
- Database normalization is the process of structuring a relational database in accordance with a series of so-called normal forms in order to reduce data redundancy and improve data integrity.
- Normalization entails organizing the columns (attributes) and tables (relations) of a database to ensure that their dependencies are properly enforced by database integrity constraints. It is accomplished by applying some formal rules either by a process of synthesis (creating a new database design) or decomposition (improving an existing database design).


## Indexes

- A SQL index is used to retrieve data from a database very fast. Indexing a table or view is, without a doubt, one of the best ways to improve the performance of queries and applications.

- A SQL index is a quick lookup table for finding records users need to search frequently. An index is small, fast, and optimized for quick lookups. It is very useful for connecting the relational tables and searching large tables.
- Notice that not only creating a primary key creates a unique SQL inde


## Transactions
A transaction is a unit of work that is performed against a database. Transactions are units or sequences of work accomplished in a logical order, whether in a manual fashion by a user or automatically by some sort of a database program.

A transaction is the propagation of one or more changes to the database. For example, if you are creating a record or updating a record or deleting a record from the table, then you are performing a transaction on that table. It is important to control these transactions to ensure the data integrity and to handle database errors.

Practically, you will club many SQL queries into a group and you will execute all of them together as a part of a transaction.

Properties of Transactions
Transactions have the following four standard properties, usually referred to by the acronym ACID.

  * Atomicity − ensures that all operations within the work unit are completed successfully. Otherwise, the transaction is aborted at the point of failure and all the previous operations are rolled back to their former state.

  * Consistency − ensures that the database properly changes states upon a successfully committed transaction.

  * Isolation − enables transactions to operate independently of and transparent to each other.

  * Durability − ensures that the result or effect of a committed transaction persists in case of a system failure.



## Locking mechanism

Locking protocols are used in database management systems as a means of concurrency control. Multiple transactions may request a lock on a data item simultaneously. Hence, we require a mechanism to manage the locking requests made by transactions. Such a mechanism is called as Lock Manager. It relies on the process of message passing where transactions and lock manager exchange messages to handle the locking and unlocking of data items.

- ### Data structure used in Lock Manager –
    The data structure required for implementation of locking is called as Lock table.

  * It is a hash table where name of data items are used as hashing index.
  * Each locked data item has a linked list associated with it.
  * Every node in the linked list represents the transaction which requested for lock, mode of lock requested (mutual/exclusive) and current status of the request (granted/waiting).
  * Every new lock request for the data item will be added in the end of linked list as a new node.
  * Collisions in hash table are handled by technique of separate chaining.

<img src="./images/Screenshot%20from%202022-06-30%2012-07-33.png"/>

- ### Explanation:
    In the above figure, the locked data items present in lock table are 5, 47, 167 and 15.

    The transactions which have requested for lock have been represented by a linked list shown below them using a downward arrow.

    Each node in linked list has the name of transaction which has requested the data item like T33, T1, T27 etc.

    The colour of node represents the status i.e. whether lock has been granted or waiting.

    Note that a collision has occurred for data item 5 and 47. It has been resolved by separate chaining where each data item belongs to a linked list. The data item is acting as header for linked list containing the locking request.

- ### Working of Lock Manager

  1. Initially the lock table is empty as no data item is locked.
  2. Whenever lock manager receives a lock request from a transaction Ti on a particular data item Qi following cases may arise:
    * If Qi is not already locked, a linked list will be created and lock will be granted to the requesting transaction Ti.
    * If the data item is already locked, a new node will be added at the end of its linked list containing the information about request made by Ti.
  3. If the lock mode requested by Ti is compatible with lock mode of transaction currently having the lock, Ti will acquire the lock too and status will be changed to ‘granted’. Else, status of Ti’s lock will be ‘waiting’.
  4. If a transaction Ti wants to unlock the data item it is currently holding, it will send an unlock request to the lock manager. The lock manager will delete Ti’s node from this linked list. Lock will be granted to the next transaction in the list.
  5. Sometimes transaction Ti may have to be aborted. In such a case all the waiting request made by Ti will be deleted from the linked lists present in lock table. Once abortion is complete, locks held by Ti will also be released.


## Database Isolation Levels
Isolation is one of the properties of SQL Transaction. Isolating/separating transactions from each other to maintain Data Integrity in Database is called Isolation.
- ### <b>Isolation Levels in SQL Server<b>

  - SQL Server provides 5 Isolation levels to implement with SQL Transaction to maintain data concurrency in the database.

  - Isolation level is nothing but locking the row while performing some task, so that other transaction can not access or will wait for the current transaction to finish its job.

  - Now, we will go through all the five Isolation levels 
    1. Read Uncommitted
        * When this level is set, the transaction can read uncommitted data resulting in the Dirty Read problem. With this isolation level, we allow a transaction to read the data which is being updated by other transaction and not yet committed.
    2. Read Committed
        * This prevents Dirty Read. When this level is set, the transaction can not read the data that is being modified by the current transaction. This will force user to wait for the current transaction to finish up its job
    3. Repeatable Read
        * This level does every work that Read Committed does. but it has one additional benefit. User A will wait for the transaction being executed by User B to execute it's Update query as well, like Read Query. But Insert query doesn't wait, this also creates Phantom Read problem.
    4. Snapshot
        * This level takes a snapshot of current data. Every transaction works on its own copy of data. When User A tries to update or insert or read anything, we ask him to re-verify the table row once again from the starting time of its execution, so that he can work on fresh data. with this level. We are not giving full faith to User A that he is going to work on fresh data but giving high-level changes of data integrity.
    5. Serializable
        * This is the maximum level of Isolation level provided by SQL Server transaction. We can prevent Phantom Read problem by implementing this level of isolation. It asks User A to wait for the current transaction for any kind of operation he wants to perform.

   - Isolation level also has a problem called "Dead Lock"- "Both the transactions lock the object and waits for each other to finish up the job". DeadLock is very dangerous because it decreases the concurrency and availability of database and the database object



## Triggers
Triggers are the SQL codes that are automatically executed in response to certain events on a particular table. These are used to maintain the integrity of the data. A trigger in SQL works similar to a real-world trigger. For example, when the gun trigger is pulled a bullet is fired.

### Advantages and Disadvantages of Triggers
- Advantages

    * Forcing security approvals on the table that are present in the database
    * Triggers provide another way to check the integrity of data
    * Counteracting invalid exchanges
    * Triggers handle errors from the database layer
    * Normally triggers can be useful for inspecting the data changes in tables
    * Triggers give an alternative way to run scheduled tasks. Using triggers, we don’t have to wait for the scheduled events to run because the triggers are invoked automatically before or after a change is made to the data in a table
 
  <br>

- Disadvantages


    * Triggers can only provide extended validations, i.e,  not all kind validations. For simple validations, you can use the NOT NULL, UNIQUE, CHECK and FOREIGN KEY constraints
    * Triggers may increase the overhead of the database
    * Triggers can be difficult to troubleshoot because they execute automatically in the database, which may not invisible to the client applications






 ## RESOURCES


  - [w3schools](https://www.w3schools.com/html/)
  - [Tutorialspoint](https://www.tutorialspoint.com/html)
  - [wikipedia](https://en.wikipedia.org/wiki/Standard_deviation)
  - [IBM SQL](https://www.ibm.com/docs/en/tivoli-netcoolimpact/7.1?topic=filters-sql)
  - [geeksforgeeks](www.geeksforgeeks.org)
  - [c-sharpcorner](https://www.c-sharpcorner.com/blogs/using-isolation-level-in-sql-transaction2)